#pragma once

#ifndef _PHRASES_H_
#define _PHRASES_H_

#include <iostream>
#include <cstdio>
#include <cstring>
#include <map>
#include <vector>
#include <queue>
#include <fstream>
#include <algorithm>

using namespace std;

bool compare(string a, string b);
int phrases(istream &input, ostream &output, int n, int m);

#endif //_PHRASES_H_