#include "phrases.h"
#include <sstream>
#include <cstdlib>

int main(int argc, char* argv[])
{
    int n = 2;
    int m = 2;
    string nameFile = "-";

    for (int i = 1; i < argc; i++)
    {
        if (strcmp(argv[i],"-n") == 0)
        {
            n = stoi(argv[i + 1]);
            i++;
        }
        else if (strcmp(argv[i], "-m") == 0)
        {
            m = stoi(argv[i + 1]);
            i++;
        }
        else
        {
                nameFile = argv[i];
        }
    };

    stringstream output;

    if (nameFile == "-")
    {
        phrases(cin, output, n, m);
    }
    else
    {
        ifstream file(nameFile);
        phrases(file, output, n, m);
        file.close();
    }

    cout << output.str();


    return 0;
}