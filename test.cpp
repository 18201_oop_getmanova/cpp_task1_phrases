#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "phrases.h"
#include <sstream>


TEST_CASE("Test_1")
{
    std::stringstream in;
    std::stringstream out;

    in << "we all live in a yellow submarine" << endl;

    for(int i = 0; i < 2; i++)
    {
        in << "yellow submarine" << endl;
    }

    phrases(in, out, 2, 3);

    REQUIRE(out.str() == "yellow submarine (3)\n");
}
TEST_CASE("test_2")
{
    std::stringstream in;
    std::stringstream out;

    for(int i = 0; i < 3; i++)
    {
        in << "one two" << endl;
    }
    for(int i = 0; i < 4; i++)
    {
        in << "three four" << endl;

    }


    phrases(in, out, 2, 3);

    REQUIRE(out.str() == "three four (4)\nfour three (3)\none two (3)\n");

}
TEST_CASE("test_3")
{
    std::stringstream in;
    std::stringstream out;

    in << "we all live in a yellow submarine" << endl;
    for(int i = 0; i < 2; i++)
    {
        in << "yellow submarine" << endl;
    }
    in << "we all live in a yellow submarine" << endl;
    for(int i = 0; i < 2; i++)
    {
        in << "yellow submarine" << endl;
    }

    phrases(in, out, 3, 4);

    REQUIRE(out.str() == "submarine yellow submarine (4)\nyellow submarine yellow (4)\n");

}

TEST_CASE("Test_4", "error") {

    stringstream in;
    stringstream out;

    in << "yellow submarine" << endl;

    phrases(in, out, 10, 4);

    REQUIRE (out.str() == "");
}
