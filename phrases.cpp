#include "phrases.h"

map <string, int> phrases_frequency;

bool compare(string a, string b)
{
    return phrases_frequency[a] < phrases_frequency[b];
};

int phrases(istream &input, ostream &output, int n, int m)
{
    vector<string> phrases_final;
    vector<string> phrases_array((unsigned)n);
    string first_str;
    int c = 0;

    for (int i = 0; i < n; i++)
    {
        input >> phrases_array[i];
        first_str += phrases_array[i] + " ";
        if (phrases_array[i] != "")
        {
            c++;
        }
    }
    if (c != n)
    {
        return 0;
    }

    if (++phrases_frequency[first_str] == m)
    {
        phrases_final.push_back(first_str);
    }

    string word;

    while (input >> word)
    {
        phrases_array.erase(phrases_array.begin());
        phrases_array.push_back(word);

        string buf_str;
        for (int i = 0; i < n; i++)
        {
            buf_str += phrases_array[i] + " ";
        }

        if (++phrases_frequency[buf_str] == m)
        {
            phrases_final.push_back(buf_str);
        }
    }

    std::sort(phrases_final.begin(),phrases_final.end(), compare);

    while (!phrases_final.empty())
    {
        output << phrases_final.back() << "(" << phrases_frequency[phrases_final.back()] << ")" << endl;
        phrases_final.pop_back();
    }

    return 0;
}